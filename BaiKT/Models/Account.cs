﻿using System.ComponentModel.DataAnnotations;

namespace BaiKT.Models
{
    public class Account
    { public int Id { get; set; }
    [Required, StringLength(50)]
    public string AccountName { get; set; }
    public int CustomerId { get; set; }
    public Customer? Customers { get; set; }
    public List<Reports> Reports { get; set; }
}
}

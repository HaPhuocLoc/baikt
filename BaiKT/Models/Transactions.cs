﻿using System.ComponentModel.DataAnnotations;

namespace BaiKT.Models
{
    public class Transactions
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }

        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }

        public int EmployeeId { get; set; }
        public Employees? Employees { get; set; }


        public List<Reports> Reports { get; set; }
        public List<Logs> Logs { get; set; }
    }
}
